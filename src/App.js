import React, { Component } from 'react';
import Header from './Components/Header/Header.jsx';
import PizzaList from './Components/PizzaList/PizzaList.jsx';

import './App.css';
import './styles/commons.css';

class App extends Component {
    state = {
        products: [
            {
                id: Math.random(),
                title: "Pizza Margaritta",
                desc: `Sauce on top, sliced fresh mozzarella, pecorino Romano & a touch of extra virgin olive oil. Add from our selection of toppings for add'l charge.`,
                price: 3.99,
                photo:`./assets/pizza1.jpg`
            },
            {
                id: Math.random(),
                title: "Pizza Napolitana",
                desc: `Our famous thin crust plum tomato pizza.`,
                price: 3.1,
                photo:`./assets/pizza2.jpg`
            },
            {
                id: Math.random(),
                title: "Pizza Hawaii",
                desc: `Plum tomato, fresh mozzarella, pesto, basil, on a honey whole wheat grandma style crust. Add from our selection of toppings for add'l charge.`,
                price: 3.2,
                photo:`./assets/pizza3.jpg`
            },
            {
                id: Math.random(),
                title: "Pizza Chili",
                desc: `Sweet & spicy Asian chicken.`,
                price: 4.5,
                photo:`./assets/pizza4.jpg`
            },
            {
                id: Math.random(),
                title: "Pizza Dolce Vita",
                desc: `Fresh mozzarella. roasted red peppers, tomato, basil, topped with a drizzle of balsamic glaze.`,
                price: 3.2,
                photo:`./assets/pizza5.jpg`
            },
            {
                id: Math.random(),
                title: "Pizza Vesuviem",
                desc: `Signature TBSONY plum tomato sauce, imported Buffalo mozzarella, basil & a touch of extra virgin olive oil. Add from our selection of toppings for add'l charge.`,
                price: 15.0,
                photo:`./assets/pizza6.jpg`
            }
        ],
        searchResults:[],
        cart:[]

    };
    componentWillMount = () => {
        this.setState({ searchResults: this.state.products });
    };

    handleSearch = event => {
        const searchQuery = event.target.value.toLowerCase();
        const searchResults = this.state.products.filter(el => {
            const searchTitleValue = el.title.toLowerCase();
            const searchDescValue = el.desc.toLowerCase();
            return (
                searchTitleValue.indexOf(searchQuery) !== -1 ||
                searchDescValue.indexOf(searchQuery) !== -1
            );
        });

        this.setState({ searchResults });
    };

    addToCart = id => {
        const currentItem = [...this.state.searchResults].filter(v => v.id === id);
        this.setState({
            ...this.state,
            cart: [...currentItem, ...this.state.cart]
        });
    };

    removeFromCart = productId => {
        const updatedCartArray = [...this.state.cart].filter(v => v.id !== productId);
        this.setState({
            ...this.state,
            cart: updatedCartArray
        });
    };

  render(){
    return(
        <div className="app-container">
          <div className="app">
              <Header
                  handleSearch={this.handleSearch}
                  cart={this.state.cart}
                  removeFromCart={this.removeFromCart}
              />
              <PizzaList
                  products={this.state.searchResults}
                  cart={this.state.cart}
                  addToCart={this.addToCart}
              />
          </div>
        </div>
    )
  }

}





export default App;