import React, { Component } from "react";
import PizzaItem from '../PizzaItem/PizzaItem.jsx';
import './PizzaList.css'

class PizzaList extends Component{
    getPizzaList = () => {
        const{products, cart, addToCart} = this.props;

        if (products.length === 0)
            return <div className="text-center w-100">No pizza's</div>;

        return products.map((product, index) => (
            <PizzaItem
                title={product.title}
                photo={product.photo}
                desc={product.desc}
                id={product.id}
                key={index}
                price={product.price}
                cart={cart}
                addToCart={addToCart}
            />
        ));

    };
    render(){
        return(
            <div className="container pizza-list">
                <div className="row d-flex justify-content-between">
                    {this.getPizzaList()}
                </div>
            </div>
        )
    }
}
export default PizzaList;