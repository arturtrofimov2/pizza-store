import React, {Component} from "react";
import './PizzaItem.css';

class PizzaItem extends Component{
    render(){
        const {title, desc, photo, price, id, addToCart} = this.props;
        return(
            <div className="mb-5 col-lg-4">
                <div className="pizza-item d-flex flex-column align-content-between">
                    <div className="pizza-item--title">{title}</div>
                    <div className="pizza-item--photo">
                        <img src={photo} alt={title} className="img-fluid"/>
                    </div>
                    <div className="pizza-item--desc">{desc}</div>
                    <div>
                    <div className="pizza-item--price">${price.toFixed(2)}</div>
                    <div className="pizza-item--order">
                        <button onClick={() => addToCart(id)}>Add to cart!</button>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default PizzaItem;