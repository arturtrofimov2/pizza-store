import React from "react";
import './ShoppingCart.css'
import Dropdown from 'react-bootstrap/Dropdown'
import ShoppingCartIcon from '../../assets/shopping-cart.png'
class ShoppingCart extends React.Component{
    getPizzaList = () => {
        const { cart, removeFromCart } = this.props;
        if (cart.length === 0)
            return <div className="text-center w-100">Your basket is empty!</div>;

        return cart.map((product, index) => (
            <div key={index} className="mb-2">
                <div className="shopping-cart-item d-flex justify-content-between align-items-center">
                    <div className="d-flex align-items-center">
                        <div className="shopping-cart-item--photo">
                            <img
                                src={product.photo}
                                alt={product.title}
                                className="img-fluid"
                            />
                        </div>
                        <div className="shopping-cart-item--title">{product.title}</div>
                    </div>
                    <div>
                        <div className="shopping-cart-item--price">
                            ${product.price.toFixed(2)}
                        </div>
                        <button className="shopping-cart-item--remove" href="#" onClick={() => removeFromCart(product.id)}>x</button>
                    </div>
                </div>
            </div>
        ));
    };
    getTotal = () => {
        const { cart } = this.props;

        return cart.reduce((acc, v) => acc + v.price, 0).toFixed(2);
    };
    render(){
        const {cart} = this.props;
        return(
            <div className="d-flex align-items-center justify-content-end">
                <span className="shopping-cart-counter">
                    {cart.length ? cart.length : ""}
                </span>
                <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                       <img src={ShoppingCartIcon} alt="Shopping Cart"/>
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <div className="shopping-cart-body">
                            {this.getPizzaList()}
                            {cart.length ? (
                                    <div className="shopping-cart-total d-flex justify-content-between align-items-center">
                                        <h3>Total:</h3>
                                        <h4>${this.getTotal()}</h4>
                                    </div>
                            ) : null}
                        </div>
                    </Dropdown.Menu>
                </Dropdown>

            </div>

        );
    }
}
export default ShoppingCart;