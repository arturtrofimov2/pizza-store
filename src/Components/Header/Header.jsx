import React, { Component } from "react";
import ShoppingCart from '../ShoppingCart/ShoppingCart.jsx';
import './Header.css'
class Header extends Component{
   render(){
       const{handleSearch, cart, removeFromCart} = this.props;
       return(
           <header className="header">
               <div className="container">
                    <h2>Pizza</h2>
                    <input type="search" placeholder="Search Pizza" onChange={handleSearch}/>
                    <div className="cart-place">
                       <ShoppingCart cart={cart} removeFromCart={removeFromCart}/>
                    </div>
               </div>
           </header>
       )
   }
}
export default Header;
